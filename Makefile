# **************************************************************************** #
#                                                                              #
#                                                         :::      ::::::::    #
#    Makefile                                           :+:      :+:    :+:    #
#                                                     +:+ +:+         +:+      #
#    By: jbard <marvin@42.fr>                       +#+  +:+       +#+         #
#                                                 +#+#+#+#+#+   +#+            #
#    Created: 2018/03/26 16:06:05 by jbard             #+#    #+#              #
#    Updated: 2018/05/10 16:25:00 by jbard            ###   ########.fr        #
#                                                                              #
# **************************************************************************** #

######################## PROJECT ########################

NAME = fillit

######################### COLOR #########################

BLUE = \033[1;34m
RED = \033[8m\033[1;31m
GREEN = \033[32m
NORMAL = \033[0m
LINE_ERASER = \033[2K\r

########################## STR ##########################

STR_SUCCESS = $(GREEN)SUCCESS$(BLUE).\n$(NORMAL)

######################### FLAGS #########################

FLAGS_DEFAULT = -Wall -Werror -Wextra -O3
FLAGS_LIB = -L$(LIBFT_PATH) -lft

####################### LIBRARIES #######################

LIBFT_PATH = libft/
LIBFT = $(LIBFT_PATH)libft.a

######################## COMMANDS #######################

ifeq ($(OS),$(filter $(OS),Darwin Linux))
CC = /usr/bin/gcc
MAKE = /usr/bin/make
RMR = /bin/rm -rf
MKDIR = /bin/mkdir
PRINTF = /usr/bin/printf
AR = /usr/bin/ar rcs
RAN = /usr/bin/ranlib
else
CC = gcc
MAKE = make
RMR = rm -rf
MKDIR = mkdir
PRINTF = printf
AR = ar rcs
RAN = ranlib
endif

######################## INCLUDES #######################

INC_DIR = ./includes/ ./libft/includes
INC_PREF = -I
INC_FILES = ./includes/fillit.h
INC	= $(addprefix $(INC_PREF), $(INC_DIR))

######################## SOURCES ########################

SRC_PATH = ./sources/
SRC_FILES = fillit_main.c\
			fillit_core.c\
			fillit_pos.c\
			fillit_alloc.c\
			fillit_check.c

SRCS = $(addprefix $(SRC_PATH), $(SRC_FILES))

######################## OBJECTS ########################

OBJ_PATH = ./objects/
OBJ_FILES = $(SRC_FILES:.c=.o)
OBJS = $(addprefix $(OBJ_PATH), $(OBJ_FILES))
OBJ_DIR = objects

######################### RULES #########################

all: $(NAME)

$(NAME): $(LIBFT) $(OBJ_DIR) $(OBJS)
	@$(PRINTF) "$(LINE_ERASER)$(RED)$@: $(BLUE)Objects compilation: $(STR_SUCCESS)"
	@$(PRINTF) "$(RED)$@: $(BLUE)Compiling project: $(NORMAL)"
	@$(CC) $(FLAGS_DEFAULT) $(OBJS) $(FLAGS_LIB) -o $@
	@$(PRINTF) "$(STR_SUCCESS)"
	@$(PRINTF) "$(RED)$@: $(GREEN)project ready.$(NORMAL)\n"

$(LIBFT):
	@$(MAKE) -sC $(LIBFT_PATH)

clean:
	@$(MAKE) clean -sC $(LIBFT_PATH)
	@$(PRINTF) "$(RED)$(NAME): $(BLUE)Cleaning objects: $(NORMAL)"
	@$(RM) $(OBJS)
	@$(PRINTF) "$(STR_SUCCESS)"

fclean: clean
	@$(MAKE) fclean -sC $(LIBFT_PATH)
	@$(PRINTF) "$(RED)$(NAME): $(BLUE)Cleaning project: $(NORMAL)"
	@$(RM) $(NAME)
	@$(PRINTF) "$(STR_SUCCESS)"

re: fclean all

$(OBJ_DIR):
	@$(PRINTF) "$(RED)$(NAME): $(BLUE)Create objects folder: $(NORMAL)"
	@$(MKDIR) -p $@
	@$(PRINTF) "$(STR_SUCCESS)"

$(OBJ_PATH)%.o: $(SRC_PATH)%.c $(INC_FILES)
	@$(PRINTF) "$(LINE_ERASER)$(RED)$(NAME): $(BLUE)Compiling: $(NORMAL)\"$@\"."
	@$(CC) -o $@ $< $(FLAGS_DEFAULT) -c $(INC)

.PHONY: all clean fclean re
