/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_core.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/19 12:55:54 by jbard             #+#    #+#             */
/*   Updated: 2017/11/29 15:56:16 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_testit(char *tetri, t_tab *tab)
{
	int		i;

	i = 1;
	while (i < 4)
	{
		if (tab->tabc[tab->x + tetri[i + 4]][tab->y + tetri[i]] != '.')
			return (0);
		i++;
	}
	return (1);
}

static void		ft_fillit(t_lstetri *l, t_tab *t)
{
	int		i;

	i = 0;
	t->xtmp = t->x;
	t->ytmp = t->y;
	while (i < 4)
	{
		t->tabc[t->x + l->tetri[i + 4]][t->y + l->tetri[i]] = l->letter;
		i++;
	}
	t->y = 0;
	t->x = 0;
}

static void		ft_unfillit(t_lstetri *l, t_tab *t)
{
	int		i;

	i = 0;
	while (i < 4)
	{
		t->tabc[t->xtmp + l->tetri[i + 4]][t->ytmp + l->tetri[i]] = '.';
		i++;
	}
	t->x = t->xtmp;
	t->y = t->ytmp;
}

int				ft_backtrackit(t_lstetri *lst, t_tab tab, int size)
{
	if (tab.tabc[tab.x][tab.y] == '.' && tab.y + lst->cmin > -1
			&& ft_testit(lst->tetri, &tab))
	{
		ft_fillit(lst, &tab);
		if (!lst->next)
			return (1);
		if (ft_backtrackit(lst->next, tab, size))
			return (1);
		ft_unfillit(lst, &tab);
	}
	while (tab.tabc[tab.x][tab.y + 1] != '.'
			&& tab.y + lst->cmax + 1 < size - 1)
		tab.y++;
	if (tab.y + lst->cmax < size - 1)
	{
		tab.y++;
		return (ft_backtrackit(lst, tab, size));
	}
	else if (tab.x + lst->lmax < size - 1)
	{
		tab.x++;
		tab.y = 0;
		return (ft_backtrackit(lst, tab, size));
	}
	return (0);
}
