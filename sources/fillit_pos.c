/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_pos.c                                       :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 11:32:54 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/29 17:46:26 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

int					check_link(char *tab, int i, int link)
{
	int k;

	while (i < 4)
	{
		k = 1;
		while (i + 4 + k < 8)
		{
			if (tab[i + k] == tab[i] && tab[i + 4 + k] == tab[i + 4] - 1)
				link++;
			if (tab[i + k] == tab[i] - 1 && tab[i + 4 + k] == tab[i + 4])
				link++;
			if (tab[i + k] == tab[i] && tab[i + 4 + k] == tab[i + 4] + 1)
				link++;
			if (tab[i + k] == tab[i] + 1 && tab[i + 4 + k] == tab[i + 4])
				link++;
			k++;
		}
		i++;
	}
	if (link == 3 || link == 4)
		return (1);
	return (0);
}

static void			ft_size_minmax(t_lstetri *lst)
{
	int i;

	i = 1;
	lst->cmax = 0;
	lst->cmin = 0;
	lst->lmax = 0;
	while (i < 4)
	{
		if (lst->tetri[i] > lst->cmax)
			lst->cmax = lst->tetri[i];
		if (lst->tetri[i] < lst->cmin)
			lst->cmin = lst->tetri[i];
		if (lst->tetri[i + 4] > lst->lmax)
			lst->lmax = lst->tetri[i + 4];
		i++;
	}
}

static t_pos		*ft_struct_pos_init(t_pos *pos)
{
	if (!(pos = malloc(sizeof(t_pos))))
		return (NULL);
	if (!(pos->tab = malloc(sizeof(char) * 9)))
		return (NULL);
	pos->tab[8] = '\0';
	pos->i = 0;
	pos->j = 0;
	pos->k = 0;
	pos->x = 0;
	pos->y = 0;
	return (pos);
}

static char			*ft_relative_position(char *str)
{
	t_pos *pos;

	pos = NULL;
	if (!(pos = ft_struct_pos_init(pos)))
		return (NULL);
	while (str[pos->i])
	{
		if (str[pos->i] == '\n')
			pos->k++;
		else if (str[pos->i] == '#' && pos->j == 0)
		{
			pos->j++;
			pos->x = pos->i - (pos->k * 5);
			pos->y = pos->k;
		}
		else if (str[pos->i] == '#' && pos->j > 0)
		{
			pos->tab[pos->j] = (pos->i - pos->x) - (pos->k * 5);
			pos->tab[pos->j + 4] = pos->k - pos->y;
			pos->j++;
		}
		pos->i++;
	}
	return (pos->tab);
}

t_lstetri			*ft_lst_pos(char **tab, int i, t_lstetri *tmp)
{
	t_lstetri	*dest;
	t_lstetri	*tmp_2;

	if (!(dest = malloc(sizeof(t_lstetri)))
			|| !(dest->tetri = ft_relative_position(tab[i]))
			|| !(check_link(dest->tetri, 0, 0)))
		return (NULL);
	ft_size_minmax(dest);
	dest->letter = 65;
	tmp = dest;
	i++;
	while (tab[i])
	{
		if (!(tmp_2 = malloc(sizeof(t_lstetri)))
				|| !(tmp_2->tetri = ft_relative_position(tab[i]))
				|| !(check_link(tmp_2->tetri, 0, 0)))
			return (NULL);
		ft_size_minmax(tmp_2);
		tmp_2->letter = i + 65;
		tmp->next = tmp_2;
		tmp = tmp_2;
		i++;
	}
	tmp->next = NULL;
	return (dest);
}
