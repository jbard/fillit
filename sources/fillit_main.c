/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_main.c                                      :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/15 14:17:51 by jbard             #+#    #+#             */
/*   Updated: 2017/12/13 16:26:10 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static char		**fillit_divide_squares(char *str, int squares)
{
	int		i;
	int		j;
	int		k;
	char	**tab;

	if (!(tab = malloc(sizeof(char*) * squares + 1)))
		return (NULL);
	i = 0;
	k = 0;
	while (i < squares)
	{
		j = 0;
		if (!(tab[i] = malloc(sizeof(char) * 21)))
			return (NULL);
		while (j < 21 && str[k])
		{
			tab[i][j] = str[k];
			j++;
			k++;
		}
		tab[i][j - 1] = '\0';
		i++;
	}
	tab[i] = NULL;
	return (tab);
}

static char		*fillit_read(char *name)
{
	int		fd;
	char	*tmp;
	char	*result;

	if ((fd = open(name, O_RDONLY)) == -1)
		return (NULL);
	if (!(tmp = ft_strnew(2))
			|| !(result = ft_strnew(0)))
		return (NULL);
	while ((read(fd, tmp, 1)) == 1)
	{
		if (!(result = ft_strjoin(result, tmp)))
			return (NULL);
	}
	close(fd);
	return (result);
}

static int		ft_exe(char *str, int size, int squares)
{
	char		**tab;
	t_lstetri	*lst;

	if (!(squares = ft_check(str)))
		return (0);
	while (squares * 4 > size * size)
		size++;
	lst = NULL;
	if (!(tab = fillit_divide_squares(str, squares))
			|| !(lst = ft_lst_pos(tab, 0, lst)))
		return (0);
	if (size < lst->cmax + 1)
		size = lst->cmax + 1;
	if (size < lst->lmax + 1)
		size = lst->lmax + 1;
	if (lst->next)
	{
		if (size < lst->next->cmax + 1)
			size = lst->next->cmax + 1;
		if (size < lst->next->lmax + 1)
			size = lst->next->lmax + 1;
	}
	if (!(ft_allocit(lst, size)))
		return (0);
	return (1);
}

int				main(int argc, char **argv)
{
	char	*str;

	if (argc != 2)
	{
		ft_putendl("usage: ./fillit file");
		return (0);
	}
	if ((str = fillit_read(argv[1])) && (ft_exe(str, 0, 0)))
		return (0);
	else
	{
		ft_putendl("error");
		exit(-1);
	}
	return (0);
}
