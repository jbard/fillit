/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_alloc.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: ppeigne <marvin@42.fr>                     +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 11:32:02 by ppeigne           #+#    #+#             */
/*   Updated: 2017/11/29 11:32:03 by ppeigne          ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static void			ft_filltab(t_tab *tab, int size)
{
	int	i;
	int	j;

	i = 0;
	while (i < size)
	{
		j = 0;
		while (j < size)
			tab->tabc[i][j++] = '.';
		tab->tabc[i][j] = '\n';
		i++;
	}
}

static void			ft_puttabc(char **tab, int size)
{
	int	i;

	i = 0;
	while (i < size)
	{
		ft_putstr(tab[i]);
		i++;
	}
}

int					ft_allocit(t_lstetri *lst, int size)
{
	t_tab	*tab;
	int		i;

	i = 0;
	if (!(tab = malloc(sizeof(t_tab))))
		return (0);
	if (!(tab->tabc = (char **)malloc(sizeof(char *) * (size + 1))))
		return (0);
	while (i < size)
	{
		if (!(tab->tabc[i] = (char *)malloc(sizeof(char) * (size + 1))))
			return (0);
		i++;
	}
	tab->x = 0;
	tab->y = 0;
	ft_filltab(tab, size);
	if (!ft_backtrackit(lst, *tab, size))
	{
		free(tab);
		return (ft_allocit(lst, size + 1));
	}
	ft_puttabc(tab->tabc, size);
	free(tab);
	return (1);
}
