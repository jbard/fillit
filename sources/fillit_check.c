/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit_check.c                                     :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/12/13 13:25:37 by jbard             #+#    #+#             */
/*   Updated: 2017/12/13 15:48:34 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "fillit.h"

static int		ft_count_squares(char *s, int sharp, int squares, int point)
{
	int i;
	int j;

	i = 0;
	while (s[i])
	{
		j = 0;
		while (s[i] && j != 20)
		{
			if (s[i] == '#')
				sharp++;
			else if (s[i] == '.')
				point++;
			i++;
			j++;
		}
		if (sharp % 4 || point % 12)
			return (0);
		squares++;
		i++;
	}
	if (squares > 26 || squares * 4 != sharp)
		return (0);
	return (squares);
}

static int		check_char(char *s)
{
	int i;

	i = 0;
	while (s[i])
	{
		if (s[i] != '#' && s[i] != '.' && s[i] != '\n')
			return (0);
		i++;
	}
	return (1);
}

static int		check_line(char *s, int squares)
{
	int i;
	int line;

	i = 0;
	line = 0;
	while (s[i])
	{
		if (((i + 1 - line / 5) % 5 == 0 && s[i] != '\n')
				|| ((i + 1) % 21 == 0 && s[i] != '\n'))
			return (0);
		if (s[i] == '\n' && ((i + 1 - line / 5) % 5 != 0 && (i + 1) % 21 != 0))
			return (0);
		if (s[i] == '\n')
			line++;
		i++;
	}
	if (((squares * 5) - 1 != line)
			|| ((i + 1) % 21 == 0 && s[i] != '\0'))
		return (0);
	return (1);
}

int				ft_check(char *str)
{
	int		squares;

	if (!check_char(str))
		return (0);
	if (!(squares = ft_count_squares(str, 0, 0, 0)))
		return (0);
	if (!check_line(str, squares))
		return (0);
	return (squares);
}
