/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   fillit.h                                           :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: jbard <marvin@42.fr>                       +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2017/11/29 17:40:49 by jbard             #+#    #+#             */
/*   Updated: 2017/12/13 15:49:32 by jbard            ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef __FILLIT_H
# define __FILLIT_H
# include <unistd.h>
# include <sys/types.h>
# include <sys/stat.h>
# include <fcntl.h>
# include <stdlib.h>
# include "libft.h"

typedef struct	s_lstetri
{
	char				*tetri;
	char				letter;
	int					cmax;
	int					cmin;
	int					lmax;
	struct s_lstetri	*next;
}				t_lstetri;
typedef struct	s_tab
{
	char				**tabc;
	int					x;
	int					y;
	int					xtmp;
	int					ytmp;
}				t_tab;
typedef struct	s_pos
{
	char				*tab;
	int					i;
	int					j;
	int					k;
	int					x;
	int					y;
}				t_pos;
int				ft_allocit(t_lstetri *lst, int size);
int				ft_backtrackit(t_lstetri *lst, t_tab tab, int size);
t_lstetri		*ft_lst_pos(char **tab, int i, t_lstetri *tmp);
int				ft_check(char *str);
#endif
